/*
 * Author: Nick K., Theresa B.
 * Purpose: MICONT Laboraufabe 3
 * Language:  C
 */
 
#include "portIO.h"

typedef enum {nichts, kaffee, tee} gewaehltesGetraenk_t;
gewaehltesGetraenk_t gewaehltesGetraenk;	
bit muenzZaehler = 0;
bit abschaltSensor = 0;

void zuruecksetzen(void) {
			setzeKaffeeAusgabe(0);
			setzeTeeAusgabe(0);
			muenzZaehler = 0;
			gewaehltesGetraenk = nichts;
			abschaltSensor = 0;
}

void leseGetraenk(void) {
	if(leseKaffeeKnopf() == 1){
			gewaehltesGetraenk = kaffee;
	}
	if(leseTeeKnopf() == 1){
			gewaehltesGetraenk = tee;
	}
}

void leseMuenze(void) {
	if(leseMuenzsensor() == 1){
			muenzZaehler = 1;
	}
}

void main(void) {
	zuruecksetzen();
	
	// Endlosschleife
	while (1) {
	
		// lese Werte an Eingaengen
		leseGetraenk();
		leseMuenze();
		
		// wenn getraenk gewaehlt und geld eingeworfen, dann beginne ausgabe
		if(muenzZaehler == 1 && gewaehltesGetraenk != nichts) {
			
			switch (gewaehltesGetraenk) {
									case kaffee:
												// kaffee ausgeben
												setzeKaffeeAusgabe(1);
												break;
									case tee:
												// tee ausgeben
												setzeTeeAusgabe(1);
												break; 
								}
			
			// warten auf abschaltsensor
			while(abschaltSensor == 0) {
				abschaltSensor = leseAbschaltsensor();
			}
			
			// zuruecksetzen
			zuruecksetzen();
		}
	}
}