sfr port4_adress = 0xE8; 

sbit port4_bit0 = port4_adress^0;
sbit port4_bit1 = port4_adress^1;
sbit port4_bit3 = port4_adress^3;
sbit port4_bit4 = port4_adress^4;
sbit port4_bit5 = port4_adress^5;
sbit port4_bit6 = port4_adress^6;

bit leseMuenzsensor(void){
		return port4_bit4;
}

bit leseAbschaltsensor(void){
		return port4_bit3;
}

bit leseKaffeeKnopf(void){
		return port4_bit5;
}

bit leseTeeKnopf(void){
		return port4_bit6;
}

void setzeKaffeeAusgabe(bit wert){
		port4_bit0 = wert;
}

void setzeTeeAusgabe(bit wert){
		port4_bit1 = wert;
}